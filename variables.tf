## General variables

variable "aws_region" {
  type = string
  default = "us-east-1"
  description = "AWS region"
}


variable "instance_type" {
  type = string
  default = "t3.micro"
  description = "AWS instance type"
}


## Bastion node variables

variable "operations_key_name" {
  type = string
  description = "Operation Key (BASTION)"
}


variable "operations_public_key_file" {
  type = string
  description = "Operation public key file"
}


## Private instances variables

variable "instance_key_name" {
  type = string
  description = "Key pair name"
}

variable "number_instances" {
  type = number
  default = 2
  description = "Number of private instances"
}
