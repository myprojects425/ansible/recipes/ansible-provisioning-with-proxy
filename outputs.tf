## Bastion
output "bastion_public_ip" {
  description = "Public ip of Bastion node"
  value       = aws_instance.bastion.public_ip
}

output "bastion_public_dns" {
  description = "Public dns of Bastion node"
  value       = aws_instance.bastion.public_dns
}

## private instaces - key-pair
output "key_pair_name" {
  description = "Instances key-pair name"
  value       = module.key_pair.key_pair_name
}

output "key_pair_fingerprint" {
  description = "The MD5 public key fingerprint as specified in section 4 of RFC 4716"
  value       = module.key_pair.key_pair_fingerprint
}

output "instances_private_dns" {
  description = "Private dns of instances"
  value       = aws_instance.instance.*.private_dns
}



output "private_key_pem" {
  description = "Private key data in PEM (RFC 1421) format"
  value       = module.key_pair.private_key_pem
  sensitive   = true
}
