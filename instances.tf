data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

# AWS module to create key-pairs
#- https://registry.terraform.io/modules/terraform-aws-modules/key-pair/aws/latest
module "key_pair" {
  source  = "terraform-aws-modules/key-pair/aws"
  version = "2.0.2"

  key_name           = var.instance_key_name
  create             = true
  create_private_key = true
}



resource "aws_instance" "instance" { # instance deployed on private subnet
  count           = var.number_instances
  ami             = data.aws_ami.ubuntu.image_id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.allow_ssh.id]
  subnet_id       = module.vpc.private_subnets[count.index % length(module.vpc.private_subnets)]
  key_name        = module.key_pair.key_pair_name

  tags = {
    Name = "instance${count.index}"
  }
}

resource "ansible_group" "instances" {
  name     = "instances"
  children = []
}

resource "ansible_host" "instance" {
  count           = var.number_instances
  name            = "instance${count.index}"
  groups          = ["instances"]

  variables  = {
    ansible_connection = "ssh"
    ansible_user  = "ubuntu"
    ansible_host = aws_instance.instance[count.index].private_dns
    #ansible_ssh_common_args = "-o ProxyCommand='ssh -p 22 -W %h:%p -q ubuntu@${aws_instance.bastion.public_dns}'"
  }
}
