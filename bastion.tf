resource "aws_instance" "bastion" {
  ami             = data.aws_ami.ubuntu.id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.allow_ssh.id]
  key_name        = var.operations_key_name
  subnet_id       = module.vpc.public_subnets[0]
  associate_public_ip_address = true

  tags = {
    Name = "BastionNode"
  }
}


resource "aws_key_pair" "operations" {
  key_name   = var.operations_key_name
  public_key = file(var.operations_public_key_file) # read operation public key file
}
